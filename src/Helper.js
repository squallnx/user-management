const crypto = require('crypto');

class Helper
{
    Md5Encrypt(str)
    {
        return crypto.createHash('md5').update(str).digest('hex');
    }
}

module.exports = new Helper();