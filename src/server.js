
require('dotenv').config();

global.CommonConstant       = require('./CommonConstant');
global.Config               = require('./Config');

const Database              = require('./Database');
const bodyParser            = require('body-parser');
const express               = require('express');
const app                   = express();

const ApiRoute              = require('./routes/Api');
const DepartmentRoute       = require('./routes/Department');
const TeamRoute             = require('./routes/Team');
const UserRoute             = require('./routes/User');
const UserRoleRoute         = require('./routes/UserRole');

Database.Connect();

app.all('*', (req, res, next) =>
{
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Credentials', 'true');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	if ('OPTIONS' == req.method)
	{
		res.sendStatus(200);
	}
	else
	{
		next();
    }
});
app.disable('x-powered-by');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(mapViewVariable);
app.use(serverLogger);
app.use(requestTimeOut);
app.use(express.static('public'));

app.engine('ejs', require('ejs-locals'));
app.set("view engine", "ejs");
app.set('views', __dirname + '/views');

//routes
app.get('/', (req, res) => res.render('index'));
app.use('/api', ApiRoute);
app.use('/department', DepartmentRoute);
app.use('/team', TeamRoute);
app.use('/user', UserRoute);
app.use('/user-role', UserRoleRoute);

app.use(handleResourceNotFound);
app.use(handleError);

app.listen(process.env.PORT || 3000, () => {
    console.log(`Server has started on ${process.env.PORT || 3000}`);
});

//map view variables
function mapViewVariable(req, res, next){
    next();
}
//server logger
const exceptExtList = ['js', 'css', 'ico', 'jpg', 'png'];
function serverLogger(req, res, next){
    let shouldLog = true;
    for(const ext of exceptExtList){
        if(req.originalUrl.endsWith('.' + ext)){
            shouldLog = false;
            break;
        }
    }
    if(req.originalUrl.startsWith('/css')
    || req.originalUrl.startsWith('/fonts')
    || req.originalUrl.startsWith('/images')
    || req.originalUrl.startsWith('/js')){
        shouldLog = false;
    }

    if(shouldLog){
        console.log(`${new Date().toGMTString()} ===> ${req.method}${' '.repeat(7 - req.method.length)}${req.originalUrl}`);
        if(Object.keys(req.body).length > 0) console.log(req.body);
    }
    next();
}
//request timeout
function requestTimeOut(req, res, next) {
	res.setTimeout(120000, () => {
        console.log(`${new Date().toGMTString()} ===> ${req.method}${' '.repeat(7 - req.method.length)}${req.originalUrl} request has timed out.`);
        res.sendStatus(408);
	})
    next();
}
//404
function handleResourceNotFound(req, res, next){
    if(req.originalUrl.startsWith('/api')){
        res.sendStatus(404);
    }
    else{
        res.render('404');
    }
}
//500
function handleError(err, req, res, next){
    console.log('');
    console.log('Caught error:');
    console.log(err.message);
    if(err.stack){
        console.log('Stack:');
        console.log(err.stack);
    }

    if(req.originalUrl.startsWith('/api')){
        res.sendStatus(500);
    }
    else{
        res.render('error');
    }
}

module.exports = app;