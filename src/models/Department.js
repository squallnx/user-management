
const {Schema, model} = require('mongoose');
const MODEL_NAME = 'Department';

const schema = new Schema({
    name: {
        type: String,
        required: [true, '{PATH} is required.'],
        unique: [true, '{PATH} is unique.'],
        default: 'Name',
        minlength: [2, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [200, '{PATH} length must be <= {MAXLENGTH}.']
    },
    headId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = model(MODEL_NAME, schema);