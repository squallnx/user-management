
const {Schema, model} = require('mongoose');
const MODEL_NAME = 'UserRole';

const schema = new Schema({
    name: {
        type: String,
        required: [true, '{PATH} is required.'],
        unique: [true, '{PATH} is unique.'],
        default: 'Role Name',
        minlength: [2, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [200, '{PATH} length must be <= {MAXLENGTH}.']
    }
});

module.exports = model(MODEL_NAME, schema);