
const {Schema, model} = require('mongoose');
const validator = require('validator');
const MODEL_NAME = 'User';

const schema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        default: 'Username',
        minlength: [2, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [30, '{PATH} length must be <= {MAXLENGTH}.'],
        validate: [v => /^[a-zA-Z][a-zA-Z0-9_\\.]{1,}$/.test(v), '{PATH} is not valid.']
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        default: 'Name',
        minlength: [2, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [50, '{PATH} length must be <= {MAXLENGTH}.']
    },
    email: {
        type: String,
        unique: [true, '{PATH} is unique.'],
        default: 'Email',
        minlength: [6, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [30, '{PATH} length must be <= {MAXLENGTH}.'],
        validate: [validator.isEmail, '{PATH} is not valid.']
    },
    roleId: {
        type: Schema.Types.ObjectId,
        required: [true, 'role is required.'],
        ref: 'UserRole'
    },
    departmentId: {
        type: Schema.Types.ObjectId,
        required: [true, 'department is required.'],
        ref: 'Department'
    },
    teamIds: {
        type: [Schema.Types.ObjectId],
        ref: 'Team',
        default: []
    }
});

module.exports = model(MODEL_NAME, schema);