
const {Schema, model} = require('mongoose');
const MODEL_NAME = 'Team';

const schema = new Schema({
    name: {
        type: String,
        required: [true, '{PATH} is required.'],
        unique: [true, '{PATH} is unique.'],
        default: 'Name',
        minlength: [2, '{PATH} length must be >= {MINLENGTH}.'],
        maxlength: [200, '{PATH} length must be <= {MAXLENGTH}.']
    },
    departmentId: {
        type: Schema.Types.ObjectId,
        required: [true, 'department is required.'],
        ref: 'Department'
    },
    userIds: {
        type: [Schema.Types.ObjectId],
        ref: 'User',
        default: []
    }
});

module.exports = model(MODEL_NAME, schema);