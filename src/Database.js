
const mongoose = require('mongoose');

class Database
{
    constructor()
    {
        const {DATABASE_USERNAME, DATABASE_PASSWORD, DATABASE_CLUSTER, DATABASE_NAME, DATABASE_NAME_TEST} = process.env;
        if(process.env.NODE_ENV === 'test'){
            this.connectionString   = process.env.MONGODB_URI || `mongodb://localhost/${DATABASE_NAME_TEST}?retryWrites=true&w=majority`;
        }
        else{
            // this.connectionString   = `mongodb+srv://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_CLUSTER}-0cskk.mongodb.net/${DATABASE_NAME}?retryWrites=true&w=majority`;
            this.connectionString   = process.env.MONGODB_URI || `mongodb://localhost/${DATABASE_NAME}?retryWrites=true&w=majority`;
        }

        this.connection         = null;
        this.db                 = null;
    }

    Connect()
    {
        mongoose
            .connect(this.connectionString, {useNewUrlParser: true})
            .catch(err => console.log(err));

        this.connection = mongoose.connection;
        this.connection.on('error', (err => console.log(err)));
        this.connection.once('open', (() => {
            this.db = this.connection.db;
            console.log('Connected to Database.');
        }));
    }
}

module.exports = new Database();