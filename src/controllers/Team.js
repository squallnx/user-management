
const BaseController = require('./Base');
const Model = require('../models/Team');
const EXCEPT_UPDATE_FIELDS = new Set([]);

class Controller extends BaseController
{
    Update(req, res, next)
    {
        if(!req.body.userIds){
            req.body.userIds = [];
        }
        super.Update(req, res, next);
    }
}

const controller = new Controller(Model, EXCEPT_UPDATE_FIELDS);
module.exports = controller;