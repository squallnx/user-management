
const BaseController = require('./Base');
const Model = require('../models/UserRole');
const EXCEPT_UPDATE_FIELDS = new Set([]);

class Controller extends BaseController
{
    
}

const controller = new Controller(Model, EXCEPT_UPDATE_FIELDS);
module.exports = controller;