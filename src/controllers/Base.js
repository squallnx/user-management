const Config = require("../Config");

class Controller
{
    constructor(model, exceptUpdateFields)
    {
        this.Model = model;
        this.exceptUpdateFields = exceptUpdateFields;

        this.FindAll = this.FindAll.bind(this);
        this.FindOne = this.FindOne.bind(this);
        this.Create = this.Create.bind(this);
        this.Update = this.Update.bind(this);
        this.Delete = this.Delete.bind(this);
    }

    ValidateModel(model)
    {
        let validSum = [];
        const err = model.validateSync();

        if(err){
            for(let key in err.errors){
                let item = err.errors[key];
                validSum.push(item.message);
            }
        }

        return validSum;
    }

    FindAll(req, res, next)
    {
        let top = req.query.top ? parseInt(req.query.top) : Config.RECORD_LIMIT;
        let option = {limit: Math.min(Config.RECORD_LIMIT, top)};
        let filter = {};

        Object.keys(req.query).forEach(key => {
            if(key != 'top'){
                filter[key] = new RegExp(req.query[key], 'i');
            }
        });

        this.Model
            .find(filter, null, option)
            .then(docs => {
                res.models = docs;
                next();
            })
            .catch(err => next(err));
    }

    FindOne(req, res, next)
    {
        this.Model
            .findOne({_id: req.params.id})
            .then(doc => {
                if(doc){
                    res.model = doc;
                    next();
                }
                else{
                    next({message: CommonConstant.INFO_MODEL_NOT_FOUND});
                }
            })
            .catch(err => next(err));
    }

    Create(req, res, next)
    {
        let model = new this.Model(req.body);

        const validSum = this.ValidateModel(model);
        if(validSum.length > 0){
            res.status(500).json({validSum});
            return;
        }

        model
            .save()
            .then(doc => {
                res.model = doc;
                next();
            })
            .catch(err => next(err));
    }

    Update(req, res, next)
    {
        this.FindOne(req, res, (error) => {
            if(error){
                if(error.message && error.message === CommonConstant.INFO_MODEL_NOT_FOUND){
                    this.Create(req, res, next);
                }
                else{
                    next(error);
                }
                return;
            }

            for(let key in req.body){
                if(!this.exceptUpdateFields.has(key)){
                    res.model[key] = req.body[key];
                }
            }

            const validSum = this.ValidateModel(res.model);
            if(validSum.length > 0){
                res.status(500).json({validSum});
                return;
            }

            res.model
                .save()
                .then(doc => {
                    next();
                })
                .catch(err => next(err));
        });
    }

    Delete(req, res, next)
    {
        this.FindOne(req, res, (error) => {
            if(error){
                next(error);
                return;
            }

            res.model
                .remove()
                .then(doc => {
                    res.model = doc;
                    next();
                })
                .catch(err => next(err));
        });
    }
}

module.exports = Controller;