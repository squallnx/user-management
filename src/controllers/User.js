
const BaseController = require('./Base');
const Helper = require('../Helper');
const Model = require('../models/User');
const EXCEPT_UPDATE_FIELDS = new Set(['username', 'password']);

class Controller extends BaseController
{
    FindOneByUsername(req, res, next)
    {
        Model
            .findOne({username: req.params['username']})
            .then(doc => {
                if(doc){
                    res.model = doc;
                    next();
                }
                else{
                    next({message: CommonConstant.INFO_MODEL_NOT_FOUND});
                }
            })
            .catch(err => next(err));
    }

    Create(req, res, next)
    {
        req.body.password = Config.DEFAULT_USER_PASSWORD;
        super.Create(req, res, next);
    }

    Update(req, res, next)
    {
        if(!req.body.teamIds){
            req.body.teamIds = [];
        }
        super.Update(req, res, next);
    }

    ResetPassword(req, res, next)
    {
        req.params.id = req.body.id;
        controller.FindOne(req, res, (error) => {
            if(error){
                next(error);
                return;
            }

            res.model.password = Config.DEFAULT_USER_PASSWORD;
            res.model
                .save()
                .then(doc => {
                    next();
                })
                .catch(err => next(err));
            })
    }

    Validate(req, res, next)
    {
        req.params.username = req.body.username;
        controller.FindOneByUsername(req, res, (err) => {
            if(err){
                next(err);
                return;
            }

            if(Helper.Md5Encrypt(req.body.password) == res.model.password){
                next();
            }
            else{
                const validSum = ['Wrong username or password.'];
                res.status(500).json({validSum});
            }
        })
    }
}

const controller = new Controller(Model, EXCEPT_UPDATE_FIELDS);
module.exports = controller;