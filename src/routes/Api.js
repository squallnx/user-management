
const express                   = require('express');
const router                    = express.Router();
const DepartmentController      = require('../controllers/Department');
const TeamController            = require('../controllers/Team');
const UserController            = require('../controllers/User');
const UserRoleController        = require('../controllers/UserRole');

let c;

//department
c = DepartmentController
router.get('/department/', c.FindAll, (req, res) => res.json(res.models));
router.get('/department/:id', c.FindOne, (req, res) => res.json(res.model));
router.post('/department/', c.Create, (req, res) => res.status(201).json(res.model));
router.patch('/department/:id', c.Update, (req, res) => res.json(res.model));
router.delete('/department/:id', c.Delete, (req, res) => res.json({message: `Deleted: ${res.model.name}`}));

//team
c = TeamController
router.get('/team/', c.FindAll, (req, res) => res.json(res.models));
router.get('/team/:id', c.FindOne, (req, res) => res.json(res.model));
router.post('/team/', c.Create, (req, res) => res.status(201).json(res.model));
router.patch('/team/:id', c.Update, (req, res) => res.json(res.model));
router.delete('/team/:id', c.Delete, (req, res) => res.json({message: `Deleted: ${res.model.name}`}));

//user
c = UserController
router.get('/user/', c.FindAll, (req, res) => res.json(res.models));
router.get('/user/:id', c.FindOne, (req, res) => res.json(res.model));
router.post('/user/', c.Create, (req, res) => res.status(201).json(res.model));
router.patch('/user/:id', c.Update, (req, res) => res.json(res.model));
router.delete('/user/:id', c.Delete, (req, res) => res.json({message: `Deleted: ${res.model.name}`}));
router.post('/user/validate', c.Validate, (req, res) => res.send(true));
router.post('/user/profile/reset-password', c.ResetPassword, (req, res) => res.send(true));

//user role
c = UserRoleController
router.get('/user-role/', c.FindAll, (req, res) => res.json(res.models));
router.get('/user-role/:id', c.FindOne, (req, res) => res.json(res.model));
router.post('/user-role/', c.Create, (req, res) => res.status(201).json(res.model));
router.patch('/user-role/:id', c.Update, (req, res) => res.json(res.model));
router.delete('/user-role/:id', c.Delete, (req, res) => res.json({message: `Deleted: ${res.model.name}`}));

module.exports = router;