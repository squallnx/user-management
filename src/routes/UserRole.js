
const express           = require('express');
const router            = express.Router();
const UserRoleController = require('../controllers/UserRole');

let c                   = UserRoleController;
router.get('/', (req, res) => res.render('user-roles'));
router.get('/:id', findOne, (req, res) => res.render('user-role', {model: res.model}));

function findOne(req, res, next){
    if(req.params.id === 'new'){
        res.model = new c.Model();
        res.model.isNew = true;
        next();
    }
    else{
        c.FindOne(req, res, next);
    } 
}

module.exports = router;