
const express           = require('express');
const router            = express.Router();
const UserController    = require('../controllers/User');
const DepartmentController    = require('../controllers/Department');
const UserRoleController    = require('../controllers/UserRole');
const TeamController    = require('../controllers/Team');

let c                   = UserController;
router.get('/', getDepartments, getRoles, (req, res) => res.render('users', {
    departments: res.departments,
    roles: res.roles,
}));
router.get('/:id', getDepartments, getRoles, getTeams, findOne, (req, res) => res.render('user', {
    model: res.model, 
    departments: res.departments,
    roles: res.roles,
    teams: res.teams,
}));

function findOne(req, res, next){
    if(req.params.id === 'new'){
        res.model = new c.Model();
        res.model.isNew = true;
        next();
    }
    else{
        c.FindOne(req, res, next);
    } 
}

function getDepartments(req, res, next){
    DepartmentController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.departments = res.models;
        next();
    })
}

function getRoles(req, res, next){
    UserRoleController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.roles = res.models;
        next();
    })
}

function getTeams(req, res, next){
    TeamController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.teams = res.models;
        next();
    })
}

module.exports = router;