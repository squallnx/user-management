
const express           = require('express');
const router            = express.Router();
const DepartmentController = require('../controllers/Department');
const UserController    = require('../controllers/User');

let c                   = DepartmentController;
router.get('/', (req, res) => res.render('departments'));
router.get('/:id', getUsers, findOne, (req, res) => res.render('department', {
    model: res.model,
    users: res.users,
}));

function findOne(req, res, next){
    if(req.params.id === 'new'){
        res.model = new c.Model();
        res.model.isNew = true;
        next();
    }
    else{
        c.FindOne(req, res, next);
    } 
}

function getUsers(req, res, next){
    UserController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.users = res.models;
        next();
    })
}

module.exports = router;