
const express           = require('express');
const router            = express.Router();
const TeamController    = require('../controllers/Team');
const DepartmentController    = require('../controllers/Department');
const UserController    = require('../controllers/User');

let c                   = TeamController;
router.get('/', (req, res) => res.render('teams'));
router.get('/:id', getDepartments, getUsers, findOne, (req, res) => res.render('team', {
    model: res.model,
    departments: res.departments,
    users: res.users,
}));

function findOne(req, res, next){
    if(req.params.id === 'new'){
        res.model = new c.Model();
        res.model.isNew = true;
        next();
    }
    else{
        c.FindOne(req, res, next);
    } 
}

function getDepartments(req, res, next){
    DepartmentController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.departments = res.models;
        next();
    })
}

function getUsers(req, res, next){
    UserController.FindAll(req, res, (err) => {
        if(err){
            next(err);
            return;
        }

        res.users = res.models;
        next();
    })
}

module.exports = router;