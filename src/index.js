const nodemon	= require('nodemon');
const path   	= require('path');

nodemon({script: path.join(__dirname, 'server.js'), ext: 'js'});

nodemon
    .on('start', () => {
        console.log('App started');
    })
    .on('quit', () => {
        console.log('App quit');
    })
    .on('restart', (files) => {
        console.log('App restarted: ', files);
    });