
class CommonConstant
{
    constructor()
    {
        this.INFO_RESOURCE_NOT_FOUND            = 'Resource not found.';
        this.INFO_MODEL_NOT_FOUND               = 'Model not found.';
        this.INFO_INVALID_MODEL                 = 'Invalid model.';
        this.INFO_ACCESS_DENIED                 = 'Access denied.';
    }
}

module.exports = new CommonConstant();