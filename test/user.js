const User = require('../src/models/User');

const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Users', () => {
	beforeEach((done) => { 
		User.remove({}, (err) => {
			done();
		});
	});

	describe('/GET user', () => {
		it('it should GET all the users', (done) => {
			chai.request(server)
				.get('/api/user')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('/GET/:id user', () => {
		it('it should GET a user by the given id', (done) => {
			let model = new User({
				username: 'newuser',
				name: 'New User',
                email: 'newuser@abc.com',
                password: '123456',
                roleId: mongoose.Types.ObjectId(),
                departmentId: mongoose.Types.ObjectId()
			});

			model.save((err, model) => {
                chai.request(server)
                    .get('/api/user/' + model.id)
                    .send(model)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name');
                        res.body.should.have.property('_id').eql(model.id);
                        done();
                    });
                });
		});
	});

	describe('/POST user', () => {
		it('it should POST a user ', (done) => {
        let model = new User({
            username: 'newuser',
            name: 'New User',
            email: 'newuser@abc.com',
            password: '123456',
            roleId: mongoose.Types.ObjectId(),
            departmentId: mongoose.Types.ObjectId()
        });
		chai.request(server)
			.post('/api/user')
			.send(model)
			.end((err, res) => {
				res.should.have.status(201);
				res.body.should.be.a('object');
				res.body.should.have.property('name');
				done();
			});
		});
	});

	describe('/PATCH/:id user', () => {
		it('it should UPDATE a user given the id', (done) => {
			let model = new User({
				username: 'newuser',
				name: 'New User',
                email: 'newuser@abc.com',
                password: '123456',
                roleId: mongoose.Types.ObjectId(),
                departmentId: mongoose.Types.ObjectId()
			});

			model.save((err, model) => {
			chai.request(server)
				.patch('/api/user/' + model.id)
				.send({
					name: 'Updated User'
				})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name').eql('Updated User');
					done();
				});
			});
		});
	});

	describe('/DELETE/:id user', () => {
		it('it should DELETE a user given the id', (done) => {
			let model = new User({
				username: 'newuser',
				name: 'New User',
                email: 'newuser@abc.com',
                password: '123456',
                roleId: mongoose.Types.ObjectId(),
                departmentId: mongoose.Types.ObjectId()
			});
			
			model.save((err, model) => {
				chai.request(server)
				.delete('/api/user/' + model.id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('message').eql('Deleted: ' + model.name);
					done();
				});
			});
		});
	});
});