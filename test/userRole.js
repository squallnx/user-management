const UserRole = require('../src/models/UserRole');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('UserRoles', () => {
	beforeEach((done) => { 
		UserRole.remove({}, (err) => {
			done();
		});
	});

	describe('/GET userRole', () => {
		it('it should GET all the userRoles', (done) => {
			chai.request(server)
				.get('/api/user-role')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('/GET/:id userRole', () => {
		it('it should GET a userRole by the given id', (done) => {
			let model = new UserRole({
				name: 'New Role'
			});

			model.save((err, model) => {
			chai.request(server)
				.get('/api/user-role/' + model.id)
				.send(model)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name');
					res.body.should.have.property('_id').eql(model.id);
					done();
				});
			});
		});
	});

	describe('/POST userRole', () => {
		it('it should POST a userRole ', (done) => {
		let model = {
			name: "New Role",
		};
		chai.request(server)
			.post('/api/user-role')
			.send(model)
			.end((err, res) => {
				res.should.have.status(201);
				res.body.should.be.a('object');
				res.body.should.have.property('name');
				done();
			});
		});
	});

	describe('/PATCH/:id userRole', () => {
		it('it should UPDATE a userRole given the id', (done) => {
			let model = new UserRole({
				name: 'New Role'
			});

			model.save((err, model) => {
			chai.request(server)
				.patch('/api/user-role/' + model.id)
				.send({
					name: 'Updated Role'
				})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name').eql('Updated Role');
					done();
				});
			});
		});
	});

	describe('/DELETE/:id userRole', () => {
		it('it should DELETE a userRole given the id', (done) => {
			let model = new UserRole({
				name: 'New Role'
			});
			
			model.save((err, model) => {
				chai.request(server)
				.delete('/api/user-role/' + model.id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('message').eql('Deleted: ' + model.name);
					done();
				});
			});
		});
	});
});