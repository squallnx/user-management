const Department = require('../src/models/Department');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Departments', () => {
	beforeEach((done) => { 
		Department.remove({}, (err) => {
			done();
		});
	});

	describe('/GET department', () => {
		it('it should GET all the departments', (done) => {
			chai.request(server)
				.get('/api/department')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('/GET/:id department', () => {
		it('it should GET a department by the given id', (done) => {
			let model = new Department({
				name: 'New Department'
			});

			model.save((err, model) => {
			chai.request(server)
				.get('/api/department/' + model.id)
				.send(model)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name');
					res.body.should.have.property('_id').eql(model.id);
					done();
				});
			});
		});
	});

	describe('/POST department', () => {
		it('it should POST a department ', (done) => {
		let model = {
			name: "New Department",
		};
		chai.request(server)
			.post('/api/department')
			.send(model)
			.end((err, res) => {
				res.should.have.status(201);
				res.body.should.be.a('object');
				res.body.should.have.property('name');
				done();
			});
		});
	});

	describe('/PATCH/:id department', () => {
		it('it should UPDATE a department given the id', (done) => {
			let model = new Department({
				name: 'New Department'
			});

			model.save((err, model) => {
			chai.request(server)
				.patch('/api/department/' + model.id)
				.send({
					name: 'Updated Department'
				})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name').eql('Updated Department');
					done();
				});
			});
		});
	});

	describe('/DELETE/:id department', () => {
		it('it should DELETE a department given the id', (done) => {
			let model = new Department({
				name: 'New Department'
			});
			
			model.save((err, model) => {
				chai.request(server)
				.delete('/api/department/' + model.id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('message').eql('Deleted: ' + model.name);
					done();
				});
			});
		});
	});
});