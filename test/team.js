const Team = require('../src/models/Team');

const mongoose = require('mongoose');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/server');
const should = chai.should();

chai.use(chaiHttp);

describe('Teams', () => {
	beforeEach((done) => { 
		Team.remove({}, (err) => {
			done();
		});
	});

	describe('/GET team', () => {
		it('it should GET all the teams', (done) => {
			chai.request(server)
				.get('/api/team')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('/GET/:id team', () => {
		it('it should GET a team by the given id', (done) => {
			let model = new Team({
				name: 'New Team',
				departmentId: mongoose.Types.ObjectId(),
			});

			model.save((err, model) => {
			chai.request(server)
				.get('/api/team/' + model.id)
				.send(model)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name');
					res.body.should.have.property('_id').eql(model.id);
					done();
				});
			});
		});
	});

	describe('/POST team', () => {
		it('it should POST a team ', (done) => {
            let model = new Team({
                name: 'New Team',
                departmentId: mongoose.Types.ObjectId(),
            });
            chai.request(server)
                .post('/api/team')
                .send(model)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name');
                    done();
                });
            });
	});

	describe('/PATCH/:id team', () => {
		it('it should UPDATE a team given the id', (done) => {
			let model = new Team({
				name: 'New Team',
				departmentId: mongoose.Types.ObjectId(),
			});

			model.save((err, model) => {
			chai.request(server)
				.patch('/api/team/' + model.id)
				.send({
					name: 'Updated Team'
				})
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('name').eql('Updated Team');
					done();
				});
			});
		});
	});

	describe('/DELETE/:id team', () => {
		it('it should DELETE a team given the id', (done) => {
			let model = new Team({
				name: 'New Team',
				departmentId: mongoose.Types.ObjectId(),
			});
			
			model.save((err, model) => {
				chai.request(server)
				.delete('/api/team/' + model.id)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');
					res.body.should.have.property('message').eql('Deleted: ' + model.name);
					done();
				});
			});
		});
	});
});