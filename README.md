# user-management

#-- USER MANAGEMENT TEST APP --#
Simple user management app for testing using Express and mongoDb.
This app can be automatically tested and deployed to Heroku using GitLab CI.

Demo link:
https://squallnx.herokuapp.com/

* Requirements:
NodeJs
mongoDb

* Environment variables (to run this app locally):
PORT=3000
DATABASE_NAME=mydb
DATABASE_NAME_TEST=mydb_test

* Scripts:
npm start
Open http://localhost:3000 to view it in the browser.

npm run dev
To run the app in dev mode with live reloading.

npm test
To test the app.