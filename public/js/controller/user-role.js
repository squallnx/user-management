
var controller = {
    init: function(){
        controller.URL = '/user-role';
        controller.API_URL = '/api' + controller.URL;

        controller.txtData = $('#txt-data');
        controller.frmDetail = $('#frm-detail');
        controller.btnSave = $('#btn-save');
        controller.btnDelete = $('#btn-delete');
    },
    registerEvents: function(){
        controller.btnSave.click(function(e){
            e.preventDefault();
            controller.save();
        });

        controller.btnDelete.click(function(e){
            e.preventDefault();
            if(confirm('Are you sure?')){
                controller.delete();
            }
        });
    },
    save: function(){
        var id = controller.txtData.data('id');
        var formObject = commonController.getFormObject(controller.frmDetail);
        var data = {
            name: formObject['name']
        };

        commonController.showLoadingWheel(true);
        $.ajax({
            type: 'PATCH',
            dataType: 'json',
            url: `${controller.API_URL}/${id}`,
            data: data,
            async: true,
            success: function(response){
                if(response._id !== id){
                    window.location.href = controller.URL;
                }
                else{
                    commonController.showLoadingWheel(false);
                }
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                if(e.responseJSON && e.responseJSON.validSum){
                    commonController.showValidSum(e.responseJSON.validSum);
                }
                else{
                    commonController.showAlertError();
                }
            }
        });
    },
    delete: function(){
        commonController.showLoadingWheel(true);

        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: `${controller.API_URL}/${controller.txtData.data('id')}`,
            data: {},
            async: true,
            success: function(response){
                commonController.showLoadingWheel(false);
                commonController.showAlertSuccess(controller.URL);
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                commonController.showAlertError();
            }
        });
    }
}

$(document).ready(function(){
    controller.init();
    controller.registerEvents();
})