
var controller = {
    init: function(){
        controller.URL = '/user-role';
        controller.API_URL = '/api' + controller.URL;
        controller.MODEL_TEMPLATE = function(model){
            return (
                `<tr class="table-row" data-id="${model._id}">
                    <td><a href="${controller.URL}/${model._id}">${model.name}</a></td>
                </tr>`
            );
        }

        controller.txtFilterName = $('#txt-filter-name');
        controller.btnNew = $('#btn-new');
        controller.tblList = $('#tbl-list');
        controller.divTblLength = $(commonController.DIV_TABLE_LENGTH_TEMPLATE);
        controller.ddlFilterListLength = controller.divTblLength.find('#ddl-list-length');
        controller.initTblList();

        controller.filter();
    },
    initTblList: function(){
        controller.tblListInstance = controller.tblList.DataTable({
            lengthMenu: commonController.TABLE_LENGTH_RANGE
        });
        $('#tbl-list_length').after(controller.divTblLength);

        return controller.tblListInstance;
    },
    registerEvents: function(){
        controller.txtFilterName
            .on('input', function(e){
                clearTimeout(controller.filterNameTimeout);
                controller.filterNameTimeout = setTimeout(function(){
                    controller.filter();
                }, commonController.TIME_AUTO_COMPLETE_FILTER);
            });
        controller.ddlFilterListLength
            .on('input', function(e){
                controller.filter();
            });

        controller.btnNew
            .click(function(e){
                e.preventDefault();
                window.location.href = controller.URL + '/new';
            });
    },
    filter: function(){
        var name = controller.txtFilterName.val();
        var top = controller.ddlFilterListLength.val();
        var url = `${controller.API_URL}?name=${name}&top=${top}`;

        commonController.showLoadingWheel(true);
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: url,
            data: {},
            async: true,
            success: function(response){
                var models = response;
                var html = '';

                models.forEach(function(model, i) {
                    html += controller.MODEL_TEMPLATE(model);
                });

                controller.tblListInstance.destroy();
                controller.tblList.find('tbody').html(html);
                controller.initTblList().draw();
                commonController.showLoadingWheel(false);
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                commonController.showAlertError();
            }
        });
    }
}

$(document).ready(function(){
    controller.init();
    controller.registerEvents();
})