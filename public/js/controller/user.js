
var controller = {
    init: function(){
        controller.URL = '/user';
        controller.API_URL = '/api' + controller.URL;

        controller.txtData = $('#txt-data');
        controller.frmDetail = $('#frm-detail');
        controller.btnSave = $('#btn-save');
        controller.btnDelete = $('#btn-delete');

        controller.ddlTeam = $('#ddl-team');
        controller.lstTeam = $('#lst-team');
        controller.btnAddTeam = $('#btn-add-team');
    },
    registerEvents: function(){
        controller.btnSave.click(function(e){
            e.preventDefault();
            controller.save();
        });

        controller.btnDelete.click(function(e){
            e.preventDefault();
            if(confirm('Are you sure?')){
                controller.delete();
            }
        });

        controller.btnAddTeam.click(function(e){
            e.preventDefault();
            controller.addTeam();
        });

        controller.registerButtonRemoveTeamClick();
    },
    registerButtonRemoveTeamClick: function(){
        $('.btn-remove-team').click(function(e){
            e.preventDefault();
            $(this).parent().remove();

            if(controller.lstTeam.find('li').length == 0)
            {
                controller.lstTeam.append($('<li>No team.</li>'));
            }
        })
    },
    save: function(){
        var id = controller.txtData.data('id');
        var formObject = commonController.getFormObject(controller.frmDetail);
        var data = {
            username: formObject['username'],
            name: formObject['name'],
            email: formObject['email'] ? formObject['email'] : undefined,
            departmentId: formObject['department'] ? formObject['department'] : undefined,
            roleId: formObject['role'] ? formObject['role'] : undefined,
            teamIds: []
        };

        controller.lstTeam
            .find('li')
            .each(function(i, item){
                if($(item).data('id')){
                    data.teamIds.push($(item).data('id'));
                }
            });

        commonController.showLoadingWheel(true);
        $.ajax({
            type: 'PATCH',
            dataType: 'json',
            url: `${controller.API_URL}/${id}`,
            data: data,
            async: true,
            success: function(response){
                if(response._id !== id){
                    window.location.href = controller.URL;
                }
                else{
                    commonController.showLoadingWheel(false);
                }
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                if(e.responseJSON && e.responseJSON.validSum){
                    commonController.showValidSum(e.responseJSON.validSum);
                }
                else{
                    commonController.showAlertError();
                }
            }
        });
    },
    delete: function(){
        commonController.showLoadingWheel(true);

        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: `${controller.API_URL}/${controller.txtData.data('id')}`,
            data: {},
            async: true,
            success: function(response){
                commonController.showLoadingWheel(false);
                commonController.showAlertSuccess(controller.URL);
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                commonController.showAlertError();
            }
        });
    },
    addTeam: function(){
        var teamId = controller.ddlTeam.val();
        var teamName = controller.ddlTeam.find('option:selected').text();
        var isExist = false;
        var lis = controller.lstTeam.find('li');
        if(lis.length == 1 && !$(lis[0]).data('id')){
            controller.lstTeam.html('');
        }
        else{
            lis.each(function(i, item){
                if($(item).data('id') == teamId){
                    isExist = true;
                }
            });
        }

        if(isExist){
            return;
        }

        controller.lstTeam
            .append(
                $(`<li data-id="${teamId}">
                    <span>${teamName}</span>
                    <a href="#" class="btn-remove-team">&nbsp;&nbsp;&nbsp;x&nbsp;&nbsp;&nbsp;</a>
                </li>`)
            );

        controller.registerButtonRemoveTeamClick();
    }
}

$(document).ready(function(){
    controller.init();
    controller.registerEvents();
})