
var controller = {
    init: function(){
        controller.URL = '/team';
        controller.API_URL = '/api' + controller.URL;

        controller.txtData = $('#txt-data');
        controller.frmDetail = $('#frm-detail');
        controller.btnSave = $('#btn-save');
        controller.btnDelete = $('#btn-delete');

        controller.ddlUser = $('#ddl-user');
        controller.lstUser = $('#lst-user');
        controller.btnAddUser = $('#btn-add-user');
    },
    registerEvents: function(){
        controller.btnSave.click(function(e){
            e.preventDefault();
            controller.save();
        });

        controller.btnDelete.click(function(e){
            e.preventDefault();
            if(confirm('Are you sure?')){
                controller.delete();
            }
        });

        controller.btnAddUser.click(function(e){
            e.preventDefault();
            controller.addUser();
        });

        controller.registerButtonRemoveUserClick();
    },
    registerButtonRemoveUserClick: function(){
        $('.btn-remove-user').click(function(e){
            e.preventDefault();
            $(this).parent().remove();

            if(controller.lstUser.find('li').length == 0)
            {
                controller.lstUser.append($('<li>No user.</li>'));
            }
        })
    },
    save: function(){
        var id = controller.txtData.data('id');
        var formObject = commonController.getFormObject(controller.frmDetail);
        var data = {
            name: formObject['name'],
            departmentId: formObject['department'] ? formObject['department'] : undefined,
            userIds: []
        };

        controller.lstUser
            .find('li')
            .each(function(i, item){
                if($(item).data('id')){
                    data.userIds.push($(item).data('id'));
                }
            });

        commonController.showLoadingWheel(true);
        $.ajax({
            type: 'PATCH',
            dataType: 'json',
            url: `${controller.API_URL}/${id}`,
            data: data,
            async: true,
            success: function(response){
                if(response._id !== id){
                    window.location.href = controller.URL;
                }
                else{
                    commonController.showLoadingWheel(false);
                }
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                if(e.responseJSON && e.responseJSON.validSum){
                    commonController.showValidSum(e.responseJSON.validSum);
                }
                else{
                    commonController.showAlertError();
                }
            }
        });
    },
    delete: function(){
        commonController.showLoadingWheel(true);

        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            url: `${controller.API_URL}/${controller.txtData.data('id')}`,
            data: {},
            async: true,
            success: function(response){
                commonController.showLoadingWheel(false);
                commonController.showAlertSuccess(controller.URL);
            },
            error: function(e){
                commonController.showLoadingWheel(false);
                commonController.showAlertError();
            }
        });
    },
    addUser: function(){
        var userId = controller.ddlUser.val();
        var userName = controller.ddlUser.find('option:selected').text();
        var isExist = false;
        var lis = controller.lstUser.find('li');
        if(lis.length == 1 && !$(lis[0]).data('id')){
            controller.lstUser.html('');
        }
        else{
            lis.each(function(i, item){
                if($(item).data('id') == userId){
                    isExist = true;
                }
            });
        }

        if(isExist){
            return;
        }

        controller.lstUser
            .append(
                $(`<li data-id="${userId}">
                    <span>${userName}</span>
                    <a href="#" class="btn-remove-user">&nbsp;&nbsp;&nbsp;x&nbsp;&nbsp;&nbsp;</a>
                </li>`)
            );

        controller.registerButtonRemoveUserClick();
    }
}

$(document).ready(function(){
    controller.init();
    controller.registerEvents();
})