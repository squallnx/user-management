
var commonController = {
    init: function(){
        commonController.TABLE_LENGTH_RANGE = [[10, 25, 50, 100, 1500], [10, 25, 50, 100, 1500]];
        commonController.DIV_TABLE_LENGTH_TEMPLATE = `
            <div class="dataTables_length" id="tbl-list-length">
                <label>&nbsp;/ page</label>
                <label>
                    of
                    <select id="ddl-list-length">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="1500">1,500</option>
                    </select>
                    entries
                </label>
            </div>
        `;
        commonController.mdlAlert = $('#mdl-alert');
        commonController.mdlAlertTitle = $('#mdl-alert-title');
        commonController.mdlAlertContent = $('#mdl-alert-content');
        commonController.loadingWheel = $('#loading-wheel');
    },
    getFormObject: function(form){
        var formArray = form.serializeArray();
        var formObject = {};
        for(let i = 0; i < formArray.length; i++)
        {
            var formItem = formArray[i];
            formObject[formItem.name] = formItem.value;
        }

        return formObject;
    },
    showAlert: function(title, content){
        commonController.mdlAlertTitle.text(title);
        commonController.mdlAlertContent.text(content);
        commonController.mdlAlert.modal('show');
    },
    showAlertSuccess: function(redirectUrl){
        commonController.showAlert('Info', 'Success.');
        if(redirectUrl){
            commonController.mdlAlert
                .off('hidden.bs.modal')
                .on('hidden.bs.modal', function(){
                    window.location.href = redirectUrl;
                });
        }
    },
    showAlertError: function(){
        commonController.showAlert('Info', 'Error.');
    },
    hideAlert: function(){
        commonController.mdlAlert.modal('hide');
    },
    showLoadingWheel: function(isShow = true){
        if(isShow)
        {
            commonController.loadingWheel.addClass('loading-wheel');
        }
        else
        {
            commonController.loadingWheel.removeClass('loading-wheel');
        }
    },
    scrollToTop: function(){
        window.scrollTo({top: 0, behavior: 'smooth'});
    },
    showValidSum: function(validSum){
        var ul = jQuery('#lst-valid-sum');
        ul.html('');
        for(let item of validSum)
        {
            var li = jQuery('<li></li>').text(item);
            ul.append(li);
        }

        commonController.scrollToTop();
    }
}

commonController.init();